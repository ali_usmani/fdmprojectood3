package com.fdmgroup.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class GameHistory {
	
	private ArrayList<Move> moves;
	HashMap<String, String> xCoordinate1 = new HashMap<String, String>();

	
	private void setUpForPrint()
	{
		xCoordinate1.put("0,0", "top left");
		xCoordinate1.put("1,0", "top");
		xCoordinate1.put("2,0", "top right");
		xCoordinate1.put("0,1", "left");
		xCoordinate1.put("1,1", "middle");
		xCoordinate1.put("2,1", "right");
		xCoordinate1.put("0,2", "bottom left");
		xCoordinate1.put("1,2", "bottom");
		xCoordinate1.put("2,2", "bottom right");
	}

	public ArrayList<Move> getMoves() {
		return moves;
	}

	public void setMoves(ArrayList<Move> moves) {
		this.moves = moves;
	}
	
	public GameHistory() {
		moves = new ArrayList<Move>();
	}
	
	public void addMove(Move move1)
	{
		moves.add(move1);
	}
	
	public String printMoves()
	{
		setUpForPrint();
		String output = "\n";
		for(Move move : moves)
		{
			output += move.getPlayer().getName() + " played the " + xCoordinate1.get(move.getxCoordinate() + "," + move.getyCoordinate()) + " location with the symbol: " + move.getPlayer().getSymbol() + "\n";
		}
		return output;
	}

}
