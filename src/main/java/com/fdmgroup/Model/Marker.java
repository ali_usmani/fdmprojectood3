package com.fdmgroup.Model;

import java.util.ArrayList;

public abstract class Marker {	
	
	private int x;
	
	private int y;
	
	
	
	@Override
	public String toString() {
		return "Marker [x=" + x + ", y=" + y + "]";
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public abstract String print();
	
	public abstract String type();

	
}
