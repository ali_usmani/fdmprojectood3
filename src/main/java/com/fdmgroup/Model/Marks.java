package com.fdmgroup.Model;

import java.util.ArrayList;

public class Marks<T extends Marker> {
	
	private ArrayList<T> markers;
	
	public Marks() {
		markers = new ArrayList<T>();
	}
	
	public void addItem(T mark) {
		markers.add(mark);
	}
	
	public ArrayList<T> getMarkers()
	{
		return markers;
	}
	
	
}
