package com.fdmgroup.Model;

import java.util.ArrayList;

public class Grid {

	private ArrayList<Cell> cells;
	
	public Grid() {
		cells = new ArrayList<Cell>();
	}

	public ArrayList<Cell> getCells() {
		return cells;
	}

	public void setCells(ArrayList<Cell> cells) {
		this.cells = cells;
	}
	
	
}
