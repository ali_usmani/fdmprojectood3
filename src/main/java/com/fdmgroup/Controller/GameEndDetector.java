package com.fdmgroup.Controller;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.fdmgroup.Model.Cell;
import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Grid;

public class GameEndDetector implements GameEndDetectorInterface {
	
	Logger logger = Logger.getLogger(GameEndDetector.class.getName());

	
	public String detect(Grid g, Game gaming, GameHistory gameHistory) throws Exception {
		
		ArrayList<Cell> cells = g.getCells();
		int[][] matrix = new int[3][3];
		int j = 0;
		for(int i = 0; i < cells.size(); i++)
		{
			if(cells.get(i).getM() != null && cells.get(i).getM().type().equals("O"))
				matrix[(i%3)][j] = 0;
			else if(cells.get(i).getM() != null && cells.get(i).getM().type().equals("X"))
				matrix[(i%3)][j] = 1;
			else if(cells.get(i).getM() == null)
				matrix[(i%3)][j] = -1;
			if(i == 2 || i == 5)
				j++;
		}
		int winner = -1;
		for(int r = 0; r < matrix.length; r++)
		{
			int colValue = matrix[0][r];
			for(int c = 0; c < matrix[r].length; c++)
			{
				if(matrix[c][r] != colValue)
					break;
				if(matrix[c][r] == colValue && c == matrix[r].length-1)
				{
					winner = colValue;
					if(winner == 0)
					{
						if(gaming.getPlayer1().getSymbol().equals("O"))
						{
							logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game horizontally (-) , row " + (r+1));
							throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game horizontally (-) , row " + (r+1));
						}
						if(gaming.getPlayer2().getSymbol().equals("O"))
						{
							logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game horizontally (-)  , row " + (r+1));
							throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game horizontally (-)  , row " + (r+1));
						}
					}
					else if(winner == 1)
					{
						if(gaming.getPlayer1().getSymbol().equals("X"))
						{
							logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game horizontally (-) , row " + (r+1));
							throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game horizontally (-) , row " + (r+1));
						}
						if(gaming.getPlayer2().getSymbol().equals("X"))
						{
							logger.info("Player: \" + gaming.getPlayer2().getName() + \", with symbol: \"+ gaming.getPlayer2().getSymbol() + \" , won the game horizontally (-), row " + (r+1));
							throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game horizontally (-) , row " + (r+1));
						}
					}
				}
			}
		}


		
		winner = -1;
		for(int c = 0; c < matrix.length; c++)
		{
			int colValue = matrix[c][0];
			for(int r = 0; r < matrix.length; r++)
			{
				if(matrix[c][r] != colValue)
					break;
				if(matrix[c][r] == colValue && r == matrix[c].length-1)
				{
					winner = colValue;
					if(winner == 0)
					{
						if(gaming.getPlayer1().getSymbol().equals("O"))
						{
							logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game vertically (|) , column " + (c+1));
							throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game vertically (|) , column " + (c+1));
						}
						if(gaming.getPlayer2().getSymbol().equals("O"))
						{
							logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game vertically (|) , column " + (c+1));
							throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game vertically (|) , column " + (c+1));
						}
					}
					else if(winner == 1)
					{
						if(gaming.getPlayer1().getSymbol().equals("X"))
						{
							logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game vertically (|) , column " + (c+1));
							throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game vertically (|) , column " + (c+1));
						}
						if(gaming.getPlayer2().getSymbol().equals("X"))
						{
							logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game vertically (|) , column " + (c+1));
							throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game vertically (|) , column " + (c+1));
						}
					}
				}
			}
		}
		
		winner = -1;
		int colValue = matrix[0][0];
		for(int d = 0; d < matrix.length; d++)
		{
			if(colValue != matrix[d][d])
				break;
			else if(matrix[d][d] == colValue && d == matrix.length-1)
			{
				winner = colValue;
				if(winner == 0)
				{
					if(gaming.getPlayer1().getSymbol().equals("O"))
					{
						logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (\\)");
						throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (\\)");
					}
					if(gaming.getPlayer2().getSymbol().equals("O"))
					{
						logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (\\)");
						throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (\\)");
					}
				}
				else if(winner == 1)
				{
					if(gaming.getPlayer1().getSymbol().equals("X"))
					{
						logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (\\)");
						throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (\\)");
					}
					if(gaming.getPlayer2().getSymbol().equals("X"))
					{
						logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (\\)");
						throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (\\)");
					}
				}
			}
		}
		
		winner = -1;
		colValue = matrix[2][0];
		for(int d = 2, c = 0; d >= 0 && c < matrix.length; d--,c++)
		{
			if(colValue != matrix[d][c])
				break;
			else if(matrix[d][c] == colValue && d == 0)
			{
				winner = colValue;
				if(winner == 0)
				{
					if(gaming.getPlayer1().getSymbol().equals("O"))
					{
						logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (/)");
						throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (/)");
					}
					if(gaming.getPlayer2().getSymbol().equals("O"))
					{
						logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (/)");
						throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (/)");
					}
				}
				else if(winner == 1)
				{
					if(gaming.getPlayer1().getSymbol().equals("X"))
					{
						logger.info("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (/)");
						throw new WinningException("Player: " + gaming.getPlayer1().getName() + ", with symbol: "+ gaming.getPlayer1().getSymbol() + " , won the game diagonally (/)");
					}
					if(gaming.getPlayer2().getSymbol().equals("X"))
					{
						logger.info("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (/)");
						throw new WinningException("Player: " + gaming.getPlayer2().getName() + ", with symbol: "+ gaming.getPlayer2().getSymbol() + " , won the game diagonally (/)");
					}
				}
			}
		}
		
		if(gameHistory.getMoves().size() == 9)
		{
			logger.info("Player: " + gaming.getPlayer1().getName() + " and "+ gaming.getPlayer2().getName() + " are tied!");
			throw new TieException("Player: " + gaming.getPlayer1().getName() + " and "+ gaming.getPlayer2().getName() + " are tied!");
		}

		return "Play On";
	}

}
