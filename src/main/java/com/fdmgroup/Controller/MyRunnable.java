package com.fdmgroup.Controller;

import com.fdmgroup.Model.Grid;
import com.fdmgroup.View.Draw;

public class MyRunnable implements Runnable {
	
	private Draw d;
	private Grid g;
	
	public MyRunnable(Draw dr, Grid gr) {
		d = dr;
		g = gr;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		System.out.print(d.draw(g));
	}

}
