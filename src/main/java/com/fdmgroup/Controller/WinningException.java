package com.fdmgroup.Controller;

public class WinningException extends Exception {
	
	public WinningException(String errorMessage)
	{
		super(errorMessage);
	}

}
