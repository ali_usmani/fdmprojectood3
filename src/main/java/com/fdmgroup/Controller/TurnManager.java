package com.fdmgroup.Controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.fdmgroup.Model.Cell;
import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Grid;
import com.fdmgroup.Model.Marker;
import com.fdmgroup.Model.Marks;
import com.fdmgroup.Model.Move;
import com.fdmgroup.Model.O_mark;
import com.fdmgroup.Model.X_mark;
import com.fdmgroup.View.Draw;
import com.fdmgroup.View.TakeInput;

public class TurnManager implements TurnManagerInterface {
	
	Logger logger = Logger.getLogger(TurnManager.class.getName());
	
	private void setChosenCell(Grid g, int r, int c, Marker m, Marks marks) {
		for(int i = 0; i < g.getCells().size(); i++)
		{
			if(g.getCells().get(i).getX() == r && g.getCells().get(i).getY() == c)
			{
				
				marks.addItem(m);
				m.setX(g.getCells().get(i).getX());
				m.setY(g.getCells().get(i).getY());
				g.getCells().get(i).setM(m);

			}
		}
	}
	
	private void placeMarkOnGridCell(Grid g, Game gaming, Move move, Marks marks)
	{
		if(gaming.getCurrentPlayer().getSymbol().equals("X"))
		{
			setChosenCell(g, move.getxCoordinate(), move.getyCoordinate(), new X_mark(), marks);
		}
		else if(gaming.getCurrentPlayer().getSymbol().equals("O"))
		{
			setChosenCell(g, move.getxCoordinate(), move.getyCoordinate(), new O_mark(), marks);
		}
	}
	
	private void swapCurrentPlayer(Game gaming)
	{
		if(gaming.getCurrentPlayer().equals(gaming.getPlayer1()))
		{
			gaming.setCurrentPlayer(gaming.getPlayer2());
		}
		else
		{
			gaming.setCurrentPlayer(gaming.getPlayer1());
		}
	}
	
	private void cleanObjectsForNewGame(Grid g, GameHistory gameHistory) {
		gameHistory.getMoves().clear();
		g.getCells().clear();
		ArrayList<Cell> cells = new ArrayList<Cell>();
		cells.add(new Cell(0,0));
		cells.add(new Cell(1,0));
		cells.add(new Cell(2,0));
		cells.add(new Cell(0,1));
		cells.add(new Cell(1,1));
		cells.add(new Cell(2,1));
		cells.add(new Cell(0,2));
		cells.add(new Cell(1,2));
		cells.add(new Cell(2,2));
		g.setCells(cells);
	}
	
	private void logGameHistoryAndFinalImage(Draw d, Grid g, GameHistory gameHistory) {
		logger.info(d.draw(g));
		logger.info(gameHistory.printMoves());
		logger.info("-----------------------------------------------------------------------------------------------------------------\n\n");
	}

	
	public boolean manageTurn(Draw d, Grid g, Game gaming, GameHistory gameHistory, Marks marks) {
		
		Move move = new Move();
		TakeInput ti = new TakeInput();
		ti.scanAndProcessMoves(gaming, gameHistory, move);
		
		placeMarkOnGridCell(g, gaming, move, marks);
		
		MyRunnable myRunnable = new MyRunnable(d,g);
		Thread thread = new Thread(myRunnable);
		thread.start();

		GameEndDetector ged = new GameEndDetector();
		try {
			ged.detect(g, gaming, gameHistory);
		} catch (WinningException we) {
			logGameHistoryAndFinalImage(d, g, gameHistory);
			we.printStackTrace();
			if(ti.continuePlaying(gaming))
			{
				cleanObjectsForNewGame(g, gameHistory);
				return true;
			}
			else
			{
				return false;
			}
		} catch (TieException te) {
			logGameHistoryAndFinalImage(d, g, gameHistory);
			te.printStackTrace();
			return false;
		} catch (Exception e) {
			logGameHistoryAndFinalImage(d, g, gameHistory);
			e.printStackTrace();
			return false;
		}
		
		swapCurrentPlayer(gaming);

		return true;
	}

}
