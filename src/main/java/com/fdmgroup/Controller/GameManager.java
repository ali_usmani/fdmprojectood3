package com.fdmgroup.Controller;

import java.util.ArrayList;
import java.util.Scanner;

import com.fdmgroup.Model.Cell;
import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Grid;
import com.fdmgroup.Model.Marker;
import com.fdmgroup.Model.Marks;
import com.fdmgroup.Model.O_mark;
import com.fdmgroup.Model.Player;
import com.fdmgroup.Model.X_mark;
import com.fdmgroup.View.Draw;
import com.fdmgroup.View.TakeInput;

public class GameManager implements GameManagerInterface {

	
	public void play() {

		
		Game gaming = new Game();
		
		Player player1 = new Player();
		player1.setPlayerNumber(1);

		TakeInput ti = new TakeInput();
		ti.takePlayerDetails(player1, gaming);
		
		Player player2 = new Player();
		player2.setPlayerNumber(2);
		
		ti.takePlayerDetails(player2, gaming);
		
		String mark = player1.getSymbol();
		Player currentPlayer = player1;
		gaming.setCurrentPlayer(currentPlayer);
		Grid g = new Grid();
		ArrayList<Cell> cells = new ArrayList<Cell>();
		cells.add(new Cell(0,0));
		cells.add(new Cell(1,0));
		cells.add(new Cell(2,0));
		cells.add(new Cell(0,1));
		cells.add(new Cell(1,1));
		cells.add(new Cell(2,1));
		cells.add(new Cell(0,2));
		cells.add(new Cell(1,2));
		cells.add(new Cell(2,2));
		g.setCells(cells);
		
		TurnManager tm = new TurnManager();
		
		
		
		Draw d = new Draw();
		
		System.out.print(d.draw());
		GameHistory gameHistory = new GameHistory();
		boolean keepPlaying = true;
		
		Marks marks = new Marks();
		
		while(keepPlaying)
		{
			keepPlaying = tm.manageTurn(d, g, gaming, gameHistory, marks);
		}
		
		// Shows Generics
//		for(Object marker : marks.getMarkers())
//		{
//			System.out.print(marker.getClass().getName());
//			System.out.println(marker.toString());
//		}
		
	}

}
