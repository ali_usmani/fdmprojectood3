package com.fdmgroup.Controller;

import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Grid;
import com.fdmgroup.Model.Marks;
import com.fdmgroup.View.Draw;

public interface TurnManagerInterface {
	
	public boolean manageTurn(Draw d, Grid g, Game gaming, GameHistory gameHistory, Marks marks);

}
