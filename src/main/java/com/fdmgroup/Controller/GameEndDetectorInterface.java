package com.fdmgroup.Controller;

import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Grid;

public interface GameEndDetectorInterface {

	
	public String detect(Grid g, Game gaming, GameHistory gameHistory) throws Exception;
	
	
	
}
