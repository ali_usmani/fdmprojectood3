package com.fdmgroup.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.fdmgroup.Model.Game;
import com.fdmgroup.Model.GameHistory;
import com.fdmgroup.Model.Move;
import com.fdmgroup.Model.Player;

public class TakeInput {
	
	private HashMap<String,Integer> xCoordinate;
	private HashMap<String,Integer> yCoordinate;
	
	public TakeInput() {
		xCoordinate = new HashMap<String,Integer>();
		xCoordinate.put("tl", 0);
		xCoordinate.put("t", 1);
		xCoordinate.put("tr", 2);
		xCoordinate.put("l", 0);
		xCoordinate.put("m", 1);
		xCoordinate.put("r", 2);
		xCoordinate.put("bl", 0);
		xCoordinate.put("b", 1);
		xCoordinate.put("br", 2);
		yCoordinate = new HashMap<String,Integer>();
		yCoordinate.put("tl", 0);
		yCoordinate.put("t", 0);
		yCoordinate.put("tr", 0);
		yCoordinate.put("l", 1);
		yCoordinate.put("m", 1);
		yCoordinate.put("r", 1);
		yCoordinate.put("bl", 2);
		yCoordinate.put("b", 2);
		yCoordinate.put("br", 2);
	}
	
	public boolean continuePlaying(Game gaming)
	{
		System.out.println(gaming.getCurrentPlayer().getName() + " , do you want to continue playing ?");
		String decision = new Scanner(System.in).nextLine();
		do
		{
			if(decision.substring(0,1).matches("([N,n])"))
				return false;
			else if(decision.substring(0,1).matches("([Y,y])"))
			{
				return true;
			}
			System.out.println(gaming.getCurrentPlayer().getName() + " , do you want to continue playing ?");
			decision = new Scanner(System.in).nextLine();
		}while(!(decision.substring(0,1).matches("([N,n])") || decision.substring(0,1).matches("([Y,y])")));
		return false;
	}
	
	private void selectFirstPlayerSymbol(Player player1)
	{
		int selectedSymbol = -1;
		while(selectedSymbol != 1 && selectedSymbol != 2)
		{
			System.out.println("Please select from the following options for your symbol:");
			System.out.println("----------------------------------");
			System.out.println("1) X");
			System.out.println("2) O");
			String answer = new Scanner(System.in).nextLine();
			if(answer.matches("([1,2].*)"))
				selectedSymbol = Integer.parseInt(answer);
			else
				continue;
			if(selectedSymbol == 1)
				player1.setSymbol("X");
			else if(selectedSymbol == 2)
				player1.setSymbol("O");
			System.out.println("\n\n");
		}
	}
	
	public void takePlayerDetails(Player player, Game gaming)
	{
		System.out.println("What is player "+ player.getPlayerNumber() +"'s full name");
		String fullName = new Scanner(System.in).nextLine();
		player.setName(fullName);
		System.out.println("\n");
		if(player.getPlayerNumber() == 1)
		{
			selectFirstPlayerSymbol(player);
			gaming.setPlayer1(player);
		}
		else if(player.getPlayerNumber() == 2)
		{
			if(gaming.getPlayer1().getSymbol().equals("X"))
				player.setSymbol("O");
			else
				player.setSymbol("X");
			gaming.setPlayer2(player);
		}

	}
	
	private boolean moveAlreadyMade(GameHistory gameHistory, int x, int y) {
		ArrayList<Move> moves = gameHistory.getMoves();
		for(Move move : moves)
		{
			if(move.getxCoordinate() == x && move.getyCoordinate() == y)
				return true;
		}
		return false;
	}
	
	public void scanAndProcessMoves(Game gaming, GameHistory gameHistory, Move move) {

		System.out.println("What is your next move "+ gaming.getCurrentPlayer().getName() +" ?");
		String input = new Scanner(System.in).nextLine();
		int x = -1;
		int y = -1;
		if(xCoordinate.get(input) != null)
			x = xCoordinate.get(input);
		if(yCoordinate.get(input) != null)
			y = yCoordinate.get(input);

		
		while(moveAlreadyMade(gameHistory, x, y) || x == -1 || y == -1)
		{
			if(x==-1 || y==-1)
			{
				System.out.println("This is not a valid move! What is your next move "+ gaming.getCurrentPlayer().getName() +" ? Format: x, y");
			}
			if(moveAlreadyMade(gameHistory, x, y))
				System.out.println("This move has already been played! What is your next move "+ gaming.getCurrentPlayer().getName() +" ? Format: x, y");
			input = new Scanner(System.in).nextLine();
			if(xCoordinate.get(input) != null)
				x = xCoordinate.get(input);
			if(yCoordinate.get(input) != null)
				y = yCoordinate.get(input);
			
		}
		
		move.setPlayer(gaming.getCurrentPlayer());
		move.setxCoordinate(x);
		move.setyCoordinate(y);
		gameHistory.addMove(move);
	}

}
