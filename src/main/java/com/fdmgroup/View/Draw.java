package com.fdmgroup.View;

import java.util.ArrayList;
import java.util.HashMap;

import com.fdmgroup.Model.Cell;
import com.fdmgroup.Model.Grid;
import com.fdmgroup.Model.Marker;

public class Draw {
	
	public Marker findMark(ArrayList<Cell> cells, int c, int r)
	{
		for(int i = 0; i < cells.size(); i++)
		{
			if(cells.get(i).getX() == c && cells.get(i).getY() == r)
				return cells.get(i).getM();
		}
		return null;
		
	}

	public String draw() {
		
		HashMap<String, String> xCoordinate = new HashMap<String, String>();
		xCoordinate.put("0,0", "tl");
		xCoordinate.put("1,0", "t");
		xCoordinate.put("2,0", "tr");
		xCoordinate.put("0,1", "l");
		xCoordinate.put("1,1", "m");
		xCoordinate.put("2,1", "r");
		xCoordinate.put("0,2", "bl");
		xCoordinate.put("1,2", "b");
		xCoordinate.put("2,2", "br");
		
		
		String firstDraw = "";
		
		firstDraw += "\n";
		int row = 0;
		firstDraw +="      ";
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				firstDraw += "|";
			}
			if(j>=0 && j<=2)
			{
				String printout = xCoordinate.get("" + j + "," + row);
				if(printout.length() == 1)
					printout += " ";
				firstDraw += printout;
			}
		}
		firstDraw += "\n";
		firstDraw += "      ";
		for(int i = 0; i < 5; i++)
		{
			firstDraw += "- ";
		}
		firstDraw += "\n";
		firstDraw += "      ";
		row = 1;
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				firstDraw += "|";
			}
			if(j>=0 && j<=2)
			{
				String printout = xCoordinate.get("" + j + "," + row);
				if(printout.length() == 1)
					printout += " ";
				firstDraw += printout;
			}
		}
		firstDraw += "\n";
		firstDraw += "      ";
		for(int i = 0; i < 5; i++)
		{
			firstDraw += "- ";
		}
		firstDraw += "\n";
		firstDraw += "      ";
		row = 2;
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				firstDraw += "|";
			}
			if(j>=0 && j<=2)
			{
				String printout = xCoordinate.get("" + j + "," + row);
				if(printout.length() == 1)
					printout += " ";
				firstDraw += printout;
			}
		}
		firstDraw += "\n\n\n";
		return firstDraw;
	}
	
	public String draw(Grid g)
	{
		String finalDraw = "";
		ArrayList<Cell> cells = g.getCells();
		
		finalDraw += "\n";
		finalDraw +="      ";
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				finalDraw += "|";
			}
			if(j == 2)
			{
				if(findMark(cells, j, 0) != null && (findMark(cells, j, 0).type().equals("X") || findMark(cells, j, 0).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 0).print();
				}
				else
					finalDraw += "  ";
			}
			else
			{
				if(findMark(cells, j, 0) != null && (findMark(cells, j, 0).type().equals("X") || findMark(cells, j, 0).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 0).print();
				}
				else
					finalDraw += " ";
			}
		}
		finalDraw += "\n";
		finalDraw += "      ";
		for(int i = 0; i < 3; i++)
		{
			finalDraw += "- ";
		}
		finalDraw += "\n";
		finalDraw += "      ";
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				finalDraw += "|";
			}
			if(j == 2)
			{
				if(findMark(cells, j, 1) != null && (findMark(cells, j, 1).type().equals("X") || findMark(cells, j, 1).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 1).print();
				}
				else
					finalDraw += "  ";
			}
			else
			{
				if(findMark(cells, j, 1) != null && (findMark(cells, j, 1).type().equals("X") || findMark(cells, j, 1).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 1).print();
				}
				else
					finalDraw += " ";
			}
		}
		finalDraw += "\n";
		finalDraw += "      ";
		for(int i = 0; i < 3; i++)
		{
			finalDraw += "- ";
		}
		finalDraw += "\n";
		finalDraw += "      ";
		for(int j = 0; j <= 3; j++)
		{
			if(j>0 && j<3)
			{
				finalDraw += "|";
			}
			if(j == 2)
			{
				if(findMark(cells, j, 2) != null && (findMark(cells, j, 2).type().equals("X") || findMark(cells, j, 2).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 2).print();
				}
				else
					finalDraw += "  ";
			}
			else
			{
				if(findMark(cells, j, 2) != null && (findMark(cells, j, 2).type().equals("X") || findMark(cells, j, 2).type().equals("O")))
				{
					finalDraw += findMark(cells, j, 2).print();
				}
				else
					finalDraw += " ";
			}
		}
		finalDraw += "\n\n";
		
		return finalDraw;
	}
}
